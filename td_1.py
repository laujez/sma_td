from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.space import MultiGrid
import random
import numpy as np
import matplotlib.pyplot as plt

#création class patient qui hérite Agent de Mesa
class PatientAgent(Agent):
    #le constructeur la class PatientAgent
    def __init__(self, id, model):
        super().__init__(id,model)#appel constructeur de la classe mère avec super()
        pathology = ['covid19', 'cancer', 'none']
        self.pathology=random.choice(pathology)
        self.name="patient n°"+ str(id)

    def step(self):
        self.nom = "Patient" + str(self.unique_id)
        print("Hello I am patient number "+ str(self.unique_id)+ " with pathology : "+ str(self.pathology))

class PhyAgent(Agent):
    # le constructeur la class PhyAgent
    def __init__(self, id, model):
        super().__init__(id, model)  # appel constructeur de la classe mère avec super()
        specialities = ['oncologue', 'pédiatre', 'cardiollogue']
        self.specialities = random.choice(specialities)
        self.name = "Physician n°" +str(id)
    
    def step(self):
        self.nom = "Personnel n°" + str(self.unique_id)
        print("Hello I am physician number "+ str(self.unique_id)+ " , i work as a : "+ str(self.specialities))

class IdAgent(Agent):
    #le constructeur la class IdAgent
    def __init__(self, id, model):
        super().__init__(id,model)#appel constructeur de la classe mère avec super()
        self.name="agent identifier n°"+ str(id)

    def step(self):
        self.nom = "identifier agent n°" + str(self.unique_id)
        print("Hello I am identifier agent ")
        print("la liste des agents du système sont:")
        for a in self.model.schedule.agents:
            if (isinstance(a, Agent)):
                print( a.name)

class SchedulerAgent(Agent):
    #le constructeur la class IdAgent
    def __init__(self, id, model):
        super().__init__(id,model)#appel constructeur de la classe mère avec super()
        self.name="agent scheduler n°"+ str(id)

    def step(self):
        self.nom = "scheduler agent n°" + str(self.unique_id)
        print("Hello I am scheduler agent "+ str(self.unique_id))


class SMA(Model):
    #le constructeur
    def __init__(self,Npatient,Nphy, L,H):
        self.nbPatientAgents=Npatient
        self.nbPhysicians=Nphy
        #ajout du planificateur qui permet de "donner la parole aux agents"
        self.schedule=RandomActivation(self) #à l'instant t ils donnent la parole aux agents de manière aléatoire
        #ajout d'une grille pour placer les agents
        self.grid = MultiGrid(L, H, torus=True)

        for i in range (self.nbPatientAgents):
            a=PatientAgent(i+1,self)
            coords=(self.random.randrange(0,L), self.random.randrange(0,H))
            #ajout d'une grille sur laquelle évoluera les agents
            self.grid.place_agent(a, coords)
            self.schedule.add(a)

        for i in range (self.nbPhysicians):
            a = PhyAgent(i + Npatient +1, self)
            coords = (self.random.randrange(0, L), self.random.randrange(0, H))
            # ajout d'une grille sur laquelle évoluera les agents
            self.grid.place_agent(a, coords)
            self.schedule.add(a)

        a = IdAgent( Npatient + 1+ Nphy, self)
        coords = (self.random.randrange(0, L), self.random.randrange(0, H))
        # ajout d'une grille sur laquelle évoluera les agents
        self.grid.place_agent(a, coords)
        self.schedule.add(a)

    def step(self):
        self.schedule.step()
        for a in self.schedule.agents:
            print(a.name+"-->"+str(isinstance(a,Agent)))


#programme principal
monModel=SMA(10,10, 10, 10)
steps=1
for i in range (steps):
    monModel.step()

agent_counts = np.zeros((monModel.grid.width, monModel.grid.height))

for cell in monModel.grid.coord_iter():
    cell_content, x, y =cell
    agent_count = len(cell_content)
    agent_counts[x][y] = agent_count

plt.imshow(agent_counts,	interpolation='nearest')
plt.colorbar()
plt.show()

